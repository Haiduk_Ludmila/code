<?php

namespace backend\modules\avtoGallery\controllers;

use Yii;
use backend\modules\avtoGallery\models\AvtoGallery;
use backend\modules\avtoGallery\models\AvtoGallerySearch;
use backend\modules\avtoGalleryImages\models\AvtoGalleryImages;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * AvtoGalleryController implements the CRUD actions for AvtoGallery model.
 */
class AvtoGalleryController extends Controller
{
    public function behaviors()
    {
        return [
			'access' => [
              'class' => \yii\filters\AccessControl::className(),
              'only' => ['index', 'create', 'view', 'update', 'delete', 'update-ru', 'upload-images'],
              'rules' => [
                [
                  'allow' => true,
                  'roles' => ['@'],
                ],
              ],
			],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all AvtoGallery models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AvtoGallerySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single AvtoGallery model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AvtoGallery model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AvtoGallery();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing AvtoGallery model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {  
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
	
	public function actionUpdateRu($id)
    {
        $avto_gallery = $this->findModel($id);

        if ($avto_gallery->load(Yii::$app->request->post()) && $avto_gallery->save()) {
		
		$avto_gallery->images = UploadedFile::getInstances($avto_gallery, 'images');

		foreach ($avto_gallery->images as $file) {
            $file->saveAs(\Yii::$app->basePath.Yii::getAlias('@avtoDownload').'/'.$avto_gallery->avto_id.'/'. $file->baseName . '.' . $file->extension);
           }
		
            return $this->redirect(['view', 'id' => $avto_gallery->id]);
        } else {
            return $this->render('_form_ru', [
                'avto_gallery' => $avto_gallery,
            ]);
        }
    }
	
	public function actionUploadImages($id)
    { 
	  $avto_gallery = $this->findModel($id);
	 
        $images = $_FILES['AvtoGallery'];
		
		if (empty($_FILES)) {
			echo json_encode(['error'=>'No files found for upload.']); 
			return; 
		}
		
		$filenames = $images['name'];
		for($i=0; $i < count($filenames); $i++){
			$ext = explode('.', basename($filenames[$i]));
			$target = \Yii::$app->basePath.Yii::getAlias('@avtoDownload').'/'.$avto_gallery->avto_id;
			if(move_uploaded_file($images['tmp_name'][$i], $target)) {
				$success = true;
				$paths[] = $target;
			} else {
				$success = false;
				break;
			}
		}
    }

    /**
     * Deletes an existing AvtoGallery model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AvtoGallery model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AvtoGallery the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AvtoGallery::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
