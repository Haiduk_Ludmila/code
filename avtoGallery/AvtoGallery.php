<?php

namespace backend\modules\avtoGallery;

use Yii;

class AvtoGallery extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\avtoGallery\controllers';

    public function init()
    {
        parent::init();
		$theme = Yii::$app->params['theme'];
        $this->viewPath = '@app/themes/'.$theme.'/modules/'. $this->id;

        // custom initialization code goes here
    }
}
