<?php

namespace backend\modules\avtoGallery\models;

use Yii;
use yii\web\UploadedFile;
use backend\modules\avtoGalleryImages\models\AvtoGalleryImages;


/**
 * This is the model class for table "lim_avto_gallery".
 *
 * @property integer $id
 * @property integer $avto_id
 * @property string $lang
 * @property string $title
 * @property string $subtitle
 * @property string $description_hover
 * @property string $title_left
 * @property string $description_left
 * @property string $title_right
 * @property string $description_right
 */
class AvtoGallery extends \yii\db\ActiveRecord
{

  public $images;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lim_avto_gallery';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['avto_id', 'lang', 'title', 'subtitle', 'description_hover', 'title_left', 'description_left', 'title_right', 'description_right'], 'required'],
            [['avto_id'], 'integer'],
            [['lang'], 'string', 'max' => 4],
            [['title', 'subtitle', 'description_hover', 'title_left', 'description_left', 'title_right', 'description_right'], 'string', 'max' => 255],
			[['images'], 'file', 'maxFiles' => 100]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'avto_id' => 'Avto ID',
            'lang' => 'Lang',
            'title' => 'Заголовок блока 2 (h2)',
            'subtitle' => 'Подзаголовок блока 2',
            'description_hover' => 'Краткое описание автомобиля (hover)',
            'title_left' => 'Заголовок левого столбца (h3)',
            'description_left' => 'Описание левого столбца',
            'title_right' => 'Заголовок правого столбца (h3)',
            'description_right' => 'Описание правого столбца',
        ];
    }
	
	public function frontAvtoView()
	{
	  return [
	    'avto_gallery_title' => $this->title,
		'avto_gallery_subtitle' => $this->subtitle,
		'avto_gallery_title_left' => $this->title_left,
		'avto_gallery_description_left' => $this->description_left,
		'avto_gallery_title_right' => $this->title_right,
		'avto_gallery_description_right' => $this->description_right,
	  ];
	}
	
	public function backDownloadImagesCreateUpdate($avto_gallery_ru_avto_id, $avto_gallery_ru_id, $model) 
	{
	  $images = UploadedFile::getInstances($this, 'images');
	  $in_order_count = AvtoGalleryImages::find()->where(['avto_gallery_id' => $avto_gallery_ru_id])->orderBy('in_order DESC')->one()->in_order;
		if ($images) {
		  foreach ($images as $file) {
            $file->saveAs(\Yii::$app->basePath.Yii::getAlias('@avtoDownload').'/'.$model->id.'/'.$file->baseName.'.'.$file->extension);

			$avto_gallery_image = new AvtoGalleryImages();
			$avto_gallery_image->avto_id = $avto_gallery_ru_avto_id;
			$avto_gallery_image->avto_gallery_id = $avto_gallery_ru_id;
			$avto_gallery_image->image = $file->baseName.'.'.$file->extension;
			$in_order_count = $in_order_count + 1;
			$avto_gallery_image->in_order = $in_order_count;
			
			$count_avto_gallery_images = AvtoGalleryImages::find()->where(['avto_gallery_id' => $avto_gallery_ru_id])->all();
			if (!$count_avto_gallery_images) {
				$avto_gallery_image->cover = 1;
				$model->cover_image = $file->baseName . '.' . $file->extension;
				$model->save(false);
			}
			$avto_gallery_image->save(false);
		  }
		}
	}
	
	public function backSaveImagesInfoUpdate($info_alt, $info_title) 
	{
	  if ($info_alt)	{	
				foreach ($info_alt as $index=>$val) 
				{ 
				  $alt = $info_alt[$index];
				  $image = AvtoGalleryImages::find()->where(['id' => $index])->one();
				  if ($alt == NULL) {
					$image->image_alt_ru = '';
				  }
				  if ($alt != NULL) {
					$image->image_alt_ru = $val;
				  }
				  $image->save(false);
				} 	
			  }	
			  
			  if ($info_title)	{	
				foreach ($info_title as $index=>$val) 
				{ 
				  $title = $info_title[$index];
				  $image = AvtoGalleryImages::find()->where(['id' => $index])->one();
				  if ($title == NULL) {
					$image->image_title_ru = '';
				  }
				  if ($title != NULL) {
					$image->image_title_ru = $val;
				  }
				  $image->save(false);
				} 	
			  }				  
	}
	
	public function backSaveInfoCreate($model_id, $lang) 
	{
	  $this->avto_id = $model_id;
	  $this->lang = $lang;
	  $this->save(false);
	}
	
	public function backLoadInfoUpdateEn($info_alt, $info_title) 
	{
	  if (!empty($info_alt))	{	
				foreach ($info_alt as $index=>$val) 
				{ 
				  $alt = $info_alt[$index];
				  $image = AvtoGalleryImages::find()->where(['id' => $index])->one();
				  if ($alt == NULL) {
					$image->image_alt_en = '';
				  }
				  if ($alt != NULL) {
					$image->image_alt_en = $val;
				  }
				  $image->save(false);
				} 	
			  }		
			
			  if ($info_title)	{	
				foreach ($info_title as $index=>$val) 
				{ 
				  $title = $info_title[$index];
				  $image = AvtoGalleryImages::find()->where(['id' => $index])->one();
				  if ($title == NULL) {
					$image->image_title_en = '';
				  }
				  if ($title != NULL) {
					$image->image_title_en = $val;
				  }
				  $image->save(false);
				} 	
			  }		
	}
}
