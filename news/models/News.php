<?php

namespace backend\modules\news\models;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use backend\modules\newsRu\models\NewsRu;
use backend\modules\newsEn\models\NewsEn;
use backend\modules\newsAlso\models\NewsAlso;
use backend\modules\mainPageNews\models\MainPageNews;
use backend\modules\avtoparkRentalGoal\models\AvtoparkRentalGoal;
use backend\modules\sitemap\models\Sitemap;
use yii\web\UploadedFile;

/**
 * This is the model class for table "lim_news".
 *
 * @property integer $id
 * @property string $url
 * @property string $main_image
 * @property integer $in_order
 * @property string $created_at
 */
class News extends \yii\db\ActiveRecord
{
  public $newsRu;
  public $newsRuTitle;
  public $news_id;
  
  public $status;
  public $title;
  public $description;
  public $main_image_alt;
  public $main_image_title;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lim_news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['url'], 'required'],
			[['url'], 'unique'],
            [['created_at', 'newsRu', 'newsRuTitle', 'title', 'news_id', 'main_image_alt', 'main_image_title'], 'safe'],
            [['url'], 'string', 'max' => 255],
			[['in_order'], 'integer'],
			[['main_image'], 'file']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'url' => 'Url новости в адресной строке',
			'main_image' => 'Главное изображение',
			'in_order' => 'Порядок',
            'created_at' => 'Дата создания',
        ];
    }
	
	public function getNewsRu()
    {
        return $this->hasMany(NewsRu::className(), ['news_id' => 'id']);
    }	
	
	public function getNewsEn()
    {
        return $this->hasMany(NewsEn::className(), ['news_id' => 'id']);
    }
	
	public function getNewsInfo($lang)
    {
      if ($lang == 'ru') return NewsRu::find()->where(['news_id' => $this->id])->one();
	  if ($lang == 'en') return NewsEn::find()->where(['news_id' => $this->id])->one();
    }
	
	public function findTitle($lang=NULL) {
	$id = $this->id;
	  if ($lang == 'ru') return NewsRu::find()->where(['news_id' => $this->id])->one()->title;
	  if ($lang == 'en') return NewsEn::find()->where(['news_id' => $this->id])->one()->title;
	}
	
	public function newUrl($same_url) {
	  $new_url = $same_url.'-'.rand(1,99);
	  $check_new_url = News::find()->where(['url' => $new_url])->one()->url;
	  if ($check_new_url)
	  $this->newUrl($check_new_url);
	  if (!$check_new_url)
	  return $new_url;
	}
	
	public function getMonth($month) {
	   if ($month == '01')
        return 'января';
	  if ($month == '02')
        return 'февраля';
	  if ($month == '03')
        return 'марта';
	  if ($month == '04')
        return 'апреля';
	  if ($month == '05')
        return 'мая';
	  if ($month == '06')
        return 'июня';
	  if ($month == '07')
        return 'июля';
	  if ($month == '08')
        return 'августа';
	  if ($month == '09')
        return 'сентября';
	  if ($month == '10')
        return 'октября';
	  if ($month == '11')
        return 'ноября';
	  if ($month == '12')
        return 'декабря';
	}	

	public function frontIndexNewsOne($news1_url, $news_count) { 
	  $padding_left = '';
	  if ($news_count%2 != 0) $padding_left = 'col-md-offset-1';
	  $news1_id = $this->id;
	  if (Yii::$app->language == 'ru')
	  $news1 = NewsRu::find()->where(['news_id' => $news1_id])->one();
	  if (Yii::$app->language == 'en')
	  $news1 = NewsEn::find()->where(['news_id' => $news1_id])->one();
	  $news1['url'] = $news1_url;
	  $news1['main_image'] = $this->main_image;
	  $news1['created_at'] = $this->created_at;
	  $news_item1 = '<div class="'.$padding_left.' col-md-5 col-sm-6">
						  <div class="carousel-card">'.Html::a(Html::img('/'.Yii::getAlias('@newsPage').'/'.$news1_id.'/main-image/'.$news1['main_image'], ['alt' => $news1['main_image_alt'], 'title' => $news1['main_image_title']]).'<span></span>', Url::to(['//news/news/view', 'url' => $news1['url']])).'
						  <div class="carousel-description">
						    <p>'.$news1['title'].'</p>
							<span class="carousel-date">'.date("d.m.Y",  strtotime($news1['created_at'])).'</span>
							<span>'.$news1['description'].'</span>
							'.Html::a(\Yii::t('basic', 'Читать далее'), Url::to(['//news/news/view', 'url' => $news1['url']])).'
						  </div>
						  </div>
						  </div>';
	  return $news_item1;				  
	}
	
	public function backDownloadImageCreate() 
	{
	  $main_image = UploadedFile::getInstance($this, 'main_image');
	  if (!empty($main_image)) {
		$this->main_image = $main_image;
		$main_image->saveAs(\Yii::$app->basePath.Yii::getAlias('@newsTextImagesDownload').'/'.$this->id.'/main-image/'.$main_image);
	  }
	  $this->save(false);
	}
	
	public function backDownloadImageUpdate($old_main_image) 
	{
	  $main_image = UploadedFile::getInstance($this, 'main_image');
	  if (!empty($main_image)) {
		$this->main_image = $main_image;
		$main_image->saveAs(\Yii::$app->basePath.Yii::getAlias('@newsTextImagesDownload').'/'.$this->id.'/main-image/'.$main_image);
	  }
	  else { $this->main_image = $old_main_image; }
	  $this->save(false);
	}
	
	public function backChangeRepeatUrl() 
	{
	  $same_url = News::find()->where(['url' => $this->url])->one()->url;
	  $count = News::find()->where(['url' => $this->url])->count();
	  if (($same_url) and ($count > 1)) {
		$new_url = $this->newUrl($same_url);
	  }
	  else $new_url = $this->url;
	  $this->url = $new_url;
	  $this->save();
	}
	
	public function backDeleteMainImage() 
	{
	    $main_image = $this->main_image;
		$image_path = \Yii::$app->basePath.'/web/images/news/'.$this->id.'/'.$main_image;
	    if (file_exists($image_path))
		unlink($image_path);
		$this->main_image = '';
		$this->save(false);
	}
	
	public function backDeleteNews($url) 
	{
	    $news_ru_one = NewsRu::find()->where(['news_id' => $this->id])->one();
		$news_ru_in_order = $news_ru_one->in_order;
		$news_ru_one->delete();
		$news_en_one = NewsEn::find()->where(['news_id' => $this->id])->one();
		$news_en_in_order = $news_en_one->in_order;
		$news_en_one->delete();
		$news_also_all = NewsAlso::find()->where(['or', ['news_id' => $this->id], ['read_also_id' => $this->id]])->all();
		foreach ($news_also_all as $news_also_one) {
		  $news_also_one->delete();
		};
		$news_ru = NewsRu::find()->where(['>', 'in_order', $news_ru_in_order])->all();
		foreach ($news_ru as $news_ru_one) {
		  $news_ru_one->in_order = $news_ru_one->in_order - 1;
		  $news_ru_one->save(false);
		}
		$news_en = NewsEn::find()->where(['>', 'in_order', $news_en_in_order])->all();
		foreach ($news_en as $news_en_one) {
		  $news_en_one->in_order = $news_en_one->in_order - 1;
		  $news_en_one->save(false);
		}
		$sitemap_url = Sitemap::find()->where(['url' => 'news/'.$url.'/'])->one();
	    $sitemap_url->delete();
		$all_avtopark_rental_goal = AvtoparkRentalGoal::find()->all();
		foreach ($all_avtopark_rental_goal as $one) {
			if ($one->news1_url == $url)
			  $one->news1_url = '';
			if ($one->news2_url == $url)
			  $one->news2_url = '';
			$one->save(false);  
		}
		$main_page_news = MainPageNews::findOne('1');
		if ($main_page_news->news1 == $url)	$main_page_news->news1 = '';
		if ($main_page_news->news2 == $url) $main_page_news->news2 = '';
		if ($main_page_news->news3 == $url) $main_page_news->news3 = '';
		if ($main_page_news->news4 == $url) $main_page_news->news4 = '';
		if ($main_page_news->news5 == $url) $main_page_news->news5 = '';
		if ($main_page_news->news6 == $url) $main_page_news->news6 = '';
		$main_page_news->save(false);
	}
}
