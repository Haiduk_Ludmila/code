<?php

namespace backend\modules\news\controllers;

use Yii;
use backend\modules\news\models\News;
use backend\modules\news\models\NewsSearch;
use backend\modules\newsRu\models\NewsRu;
use backend\modules\newsRu\models\NewsRuSearch;
use backend\modules\newsEn\models\NewsEn;
use backend\modules\newsEn\models\NewsEnSearch;
use backend\modules\newsAlso\models\NewsAlso;
use backend\modules\mainPageNews\models\MainPageNews;
use backend\modules\avtoparkRentalGoal\models\AvtoparkRentalGoal;
use backend\modules\sitemap\models\Sitemap;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * NewsController implements the CRUD actions for News model.
 */
class NewsController extends Controller
{
    public function behaviors()
    {
        return [
			'access' => [
              'class' => \yii\filters\AccessControl::className(),
              'only' => ['index', 'create', 'view', 'update', 'delete', 'image-get',
						 'image-upload', 'delete-main-image'],
              'rules' => [
                [
                  'allow' => true,
                  'roles' => ['@'],
                ],
              ],
			],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
	
	public function actions()
    {
        return [
			'images-get' => [
				'class' => 'vova07\imperavi\actions\GetAction',
				'url' => '/backend'.Yii::getAlias('@newsTextImagesDownload'), // URL адрес папки где хранятся изображения.
				'path' => '@webroot/images/news/', // Или абсолютный путь к папке с изображениями.
				'type' => \vova07\imperavi\actions\GetAction::TYPE_IMAGES,
			],
			'image-upload' => [
				'class' => 'vova07\imperavi\actions\UploadAction',
				'url' => '/backend'.Yii::getAlias('@newsTextImagesDownload'),
				'path' => '@webroot/images/news/', // Или абсолютный путь к папке куда будут загружатся изображения.
				'unique' => false
			],
        ];
    }	

    public function actionCreate()
    {
	  
	  $rowCount = $_GET['rowCount'];
	  if (empty($_GET['rowCount'])) $rowCount = 10;
	
	  $url_parts = explode("/", $_SERVER[REQUEST_URI]);
	  $action = $url_parts[3];
	  $lang = 'ru';
	  
	  $new_id = News::find()->orderBy('id DESC')->one()->id + 1;
	  
        $model = new News();
		$model_ru = new NewsRu();
		$model_en = new NewsEn();
		
		$searchModel = new NewsRuSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams, $rowCount);
		
		$model->in_order = 1;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
		
		  $news = News::find()->where(['!=', 'id', $model->id])->all();
		  foreach ($news as $news_one) {
			  $news_one->in_order = $news_one->in_order + 1;
			  $news_one->save(false);
		  }
		
		  $new_folder = \Yii::$app->basePath.Yii::getAlias('@newsTextImagesDownload').'/'.$model->id;
		  $new_folder_main_image = \Yii::$app->basePath.Yii::getAlias('@newsTextImagesDownload').'/'.$model->id.'/main-image';
		  if(!is_dir($new_folder)) {
		    mkdir($new_folder,0777, true);
		    chmod($new_folder, 0777);
	      }
		  if(!is_dir($new_folder_main_image)) {
		    mkdir($new_folder_main_image,0777, true);
		    chmod($new_folder_main_image, 0777);
	      }
		  $model->backDownloadImageCreate();
			$news_ru = NewsRu::find()->where(['!=', 'news_id', $model->id])->all();
			foreach ($news_ru as $news_ru_one) {
			  $news_ru_one->in_order = $news_ru_one->in_order + 1;
			  $news_ru_one->save(false);
			}
			$model_ru->backLoadInfoNewsCreate($model->id, $model->url);
			$news_en = NewsEn::find()->where(['!=', 'news_id', $model->id])->all();
			foreach ($news_en as $news_en_one) {
			  $news_en_one->in_order = $news_en_one->in_order + 1;
			  $news_en_one->save(false);
			}
			$model_en->backLoadInfoNewsCreate($model->id, $_POST['NewsRu']);
			if ($_POST['selection']) {
			  
			  foreach ($_POST['selection'] as $index=>$val) {
			    $read_also = new NewsAlso();
				$read_also->backLoadInfoNewsCreate($index, $val, $model_ru->news_id);
			  }
		    }
			/*** SITEMAP > ***/
				$sitemap = new Sitemap();
				$sitemap->url = 'news/'.$model->url.'/';
				$sitemap->save(false);
		    /*** < SITEMAP ***/	
            return $this->redirect(['update', 'id' => $model->id, 'lang' => 'ru']);
        } else {
            return $this->render('create', [
                'model' => $model,
				'model_ru' => $model_ru,
				'dataProvider' => $dataProvider,
				'action' => $action,
				'lang' => $lang,
				'new_id' => $new_id,
				'rowCount' => $rowCount
            ]);
        }
    }

    public function actionUpdate($id, $lang)
    { 
	
	  $rowCount = $_GET['rowCount'];
	  if (empty($_GET['rowCount'])) $rowCount = 10;
	  
      $url_parts = explode("/", $_SERVER[REQUEST_URI]);
	  $action = $url_parts[3];
	  
        $model = News::findOne($id);
		$old_url = $model->url;
		
		if ($lang == 'ru') {
		  $model_add = NewsRu::find()->where(['news_id' => $id])->one();
		  $searchModel = new NewsRuSearch();
		}
		if ($lang == 'en') {
		  $model_add = NewsEn::find()->where(['news_id' => $id])->one();
		  $searchModel = new NewsEnSearch();
		}
		if (empty($model_add))
		throw new NotFoundHttpException('The requested page does not exist.');
		
		$model_id = $model_add->news_id;
		
        $dataProvider = $searchModel->searchNewsAlsoList(Yii::$app->request->queryParams, $model_id, $rowCount);
		
		$old_main_image = $model->main_image;
		
		if ($model->load(Yii::$app->request->post())) {
			//$model->backChangeRepeatUrl();
			$same_url = News::find()->where(['url' => $model->url])->one()->url;
			$count = News::find()->where(['url' => $model->url])->count();
			if (($same_url) and ($count > 1)) {
		      $new_url = $model->newUrl($same_url);
			}
			else $new_url = $model->url;
			$model->url = $new_url;
			
			$model->save();
			
			if ($old_url != $new_url) {
			  $main_page_news = MainPageNews::findOne('1');
			  $main_page_news->backChangeUrl($old_url, $new_url);
			}
			$model->backDownloadImageUpdate($old_main_image);
		}	
			
		if ($model_add->load(Yii::$app->request->post()) && $model_add->save(false)) {	

			if ($_POST['selection']) {
			  $all_read_also = NewsAlso::find()->where(['news_id' => $model_add->news_id, 'lang' => $lang])->all();
			  foreach ($_POST['selection'] as $index=>$val) {
			   $is_read_also = NewsAlso::find()->where(['news_id' => $model_add->news_id, 'read_also_id' => $val, 'lang' => $lang])->one();
			   if (empty($is_read_also)) {
			    $read_also = new NewsAlso();
			    $read_also->news_id = $model_add->news_id;
			    $read_also->read_also_id = $val;
				$read_also->lang = $lang;
			    $read_also->save(false);
			   }
			   $all_vals_array[] = $val;
			  }
			  
			  foreach ($all_read_also as $one_read_also) {
			    $all_read_also_array[] = $one_read_also->id;
			  }
			  
			  foreach ($all_read_also as $one_read_also) {
			    foreach ($all_vals_array as $index=>$val) {
				  if ($one_read_also->read_also_id == $val)
				  $last_array[] = $one_read_also->read_also_id;
				}
				
				if (!in_array($one_read_also->read_also_id, $last_array)) {
				    $one = NewsAlso::find()->where(['id' => $one_read_also->id])->one();
					$one->delete();
				}
			  }
		    }
			if (!$_POST['selection']) {
			  $all_read_also = NewsAlso::find()->where(['news_id' => $model_add->news_id, 'lang' => $lang])->all();
			  foreach ($all_read_also as $one) {
			    $one->delete();
			  }	
			}
			
			/*** CATEGORY RENTAL GOAL > ***/
			if ($old_url != $model->url) {
			  $all_avtopark_rental_goal = AvtoparkRentalGoal::find()->all();
			  foreach ($all_avtopark_rental_goal as $one) {
			    if ($one->news1_url == $old_url)
				  $one->news1_url = $model->url;
				if ($one->news2_url == $old_url)
				  $one->news2_url = $model->url;
				$one->save(false);  
			  }
			}
			/*** < CATEGORY RENTAL GOAL ***/
			
			/*** SITEMAP > ***/
			if ($old_url != $model->url) {
			  $sitemap_url = Sitemap::find()->where(['url' => 'news/'.$old_url.'/'])->one();
			  $sitemap_url->url = 'news/'.$model->url.'/';
			  $sitemap_url->save(false);
			}
		/*** < SITEMAP ***/
		
            return $this->redirect(['update', 'id' => $model->id, 'lang' => $lang]);
        } else {
            return $this->render('update', [
                'model' => $model,
				'model_add' => $model_add,
				'searchModel' => $searchModel,
				'dataProvider' => $dataProvider,
				'action' => $action,
				'lang' => $lang,
				'rowCount' => $rowCount
            ]);
        }
    }
	
	public function actionDeleteMainImage()
    {
	  $this->layout = false;
	  $url =  $_POST['News']['url'];
	  $lang = 'ru';
        $model = News::find()->where(['url' => $url])->one();
		$model->backDeleteMainImage();
		return $this->redirect(['update', 'id' => $model->id, 'lang' => $lang]);

    }

    public function actionDelete()
    {
		$id = $_POST['id'];
		$model = News::findOne($id);
		$url = $model->url;
		$model->backDeleteNews($url);
		$model->backDeleteMainImage();
		$model->delete();
    }

    protected function findModel($id)
    {
        if (($model = News::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
