<?php

namespace backend\modules\avto;

use Yii;

class Avto extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\avto\controllers';

    public function init()
    {
        parent::init();
		$theme = Yii::$app->params['theme'];
        $this->viewPath = '@app/themes/'.$theme.'/modules/'. $this->id;

        // custom initialization code goes here
    }
}
