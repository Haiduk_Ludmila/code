<?php

namespace backend\modules\avto\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\avto\models\Avto;

/**
 * AvtoSearch represents the model behind the search form about `backend\modules\avto\models\Avto`.
 */
class AvtoSearch extends Avto
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'brand_id', 'in_order', 'status'], 'integer'],
            [['title_ru', 'subtitle_ru', 'main_image', 'main_image_2x', 'cover_image', 'title_en', 'subtitle_en', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $category, $rowCount=NULL)
    {
		if ($rowCount == NULL) $rowCount = $_GET['rowCount'];
		
        $query = Avto::find()->where(['category_url' => $category]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'sort'=> ['defaultOrder' => ['in_order'=>SORT_ASC]],
			'pagination' => [
					'pageSize' => $rowCount,
                    'validatePage' => false,
                ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'brand_id' => $this->brand_id,
            'created_at' => $this->created_at,
            'in_order' => $this->in_order,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'title_ru', $this->title_ru])
            ->andFilterWhere(['like', 'subtitle_ru', $this->subtitle_ru])
            ->andFilterWhere(['like', 'main_image', $this->main_image])
            ->andFilterWhere(['like', 'main_image_2x', $this->main_image_2x])
            ->andFilterWhere(['like', 'cover_image', $this->cover_image])
            ->andFilterWhere(['like', 'title_en', $this->title_en])
            ->andFilterWhere(['like', 'subtitle_en', $this->subtitle_en]);

        return $dataProvider;
    }
}
