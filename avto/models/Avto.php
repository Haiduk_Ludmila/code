<?php

namespace backend\modules\avto\models;

use Yii;
use yii\web\UploadedFile;
use yii\helpers\Html;
use backend\modules\avto\models\Avto;
use backend\modules\avtoGallery\models\AvtoGallery;
use backend\modules\avtoGalleryImages\models\AvtoGalleryImages;
use backend\modules\avtoExperience\models\AvtoExperience;
use backend\modules\avtoAdvantages\models\AvtoAdvantages;
use backend\modules\avtoAdvantagesItems\models\AvtoAdvantagesItems;
use backend\modules\avtoPrice\models\AvtoPrice;
use backend\modules\avtoparkRentalLider\models\AvtoparkRentalLider;
use backend\modules\sitemap\models\Sitemap;

/**
 * This is the model class for table "lim_avto".
 *
 * @property integer $id
 * @property string $title_ru
 * @property string $subtitle_ru
 * @property string $price_ru
 * @property string $main_image
 * @property string $main_image_2x
 * @property string $cover_image
 * @property integer $brand_id
 * @property string $title_en
 * @property string $subtitle_en
 * @property string $price_en
 * @property string $created_at
 * @property integer $in_order
 * @property integer $status
 */
class Avto extends \yii\db\ActiveRecord
{

  public $avto_id;
  public $lang;
  public $title;
  public $subtitle;
  public $description_hover;
  public $title_left;
  public $description_left;
  public $title_right;
  public $description_right;
  public $images;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lim_avto';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['title_ru', 'subtitle_ru', 'main_image', 'main_image_2x', 'cover_image', 'brand_id', 'title_en', 'subtitle_en', 'in_order', 'status'], 'required'],
            [['brand_id', 'in_order', 'status'], 'integer'],
            [['created_at'], 'safe'],
            [['url', 'title_ru', 'subtitle_ru', 'price_ru', 'main_image', 'main_image_2x', 'cover_image', 'cover_description_ru', 'cover_description_en', 'title_en', 'subtitle_en', 'price_en', 'meta_title_ru', 'meta_description_ru', 'meta_keywords_ru', 'meta_title_en', 'meta_description_en', 'meta_keywords_en'], 'string', 'max' => 255],
			[['images'], 'file', 'maxFiles' => 100]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
			'url' => 'Url автомобиля',
            'title_ru' => 'Название автомобиля(h1)',
            'subtitle_ru' => 'Подзаголовок автомобиля',
			'price_ru' => 'Цена автомобиля',
            'main_image' => 'Main Image',
            'main_image_2x' => 'Main Image 2x',
            'cover_image' => 'Cover Image',
            'brand_id' => 'Выбор фильтра',
            'title_en' => 'Название автомобиля(h1)',
            'subtitle_en' => 'Подзаголовок автомобиля',
			'price_en' => 'Цена автомобиля',
            'created_at' => 'Created At',
            'in_order' => 'In Order',
            'status' => 'Status',
			'meta_title_ru' => 'Заголовок страницы (Title)',
            'meta_description_ru' => 'Описание страницы (Description)',
            'meta_keywords_ru' => 'Ключевые слова страницы (Keywords)',
            'meta_title_en' => 'Заголовок страницы (Title)',
            'meta_description_en' => 'Описание страницы (Description)',
            'meta_keywords_en' => 'Ключевые слова страницы (Keywords)',
        ];
    }
	
	public function arrowOrder($category) {
		$model_last = Avto::find()->where(['category_url' => $category])->orderBy('in_order desc')->one();
		$model_id_last = $model_last->id;
		$model_first = Avto::find()->where(['category_url' => $category])->orderBy('in_order asc')->one();
		$model_id_first = $model_first->id;
		if ($model_id_first == $model_id_last)
        return '<span class="span-in-order inlineEdit '.$this->in_order.'">'.$this->in_order.'</span>';
		if ($model_id_first == $this->id)
        return '<span class="span-in-order inlineEdit '.$this->in_order.'">'.$this->in_order.'</span>'.Html::a('', '', ['class' => 'in-order-up '.$this->in_order]);
		if ($model_id_last == $this->id)
        return '<span class="span-in-order inlineEdit '.$this->in_order.'">'.$this->in_order.'</span>'.Html::a('', '', ['class' => 'in-order-down '.$this->in_order]);
		if (($model_id_first != $this->id) && ($model_id_last != $this->id))
        return '<span class="span-in-order inlineEdit '.$this->in_order.'">'.$this->in_order.'</span>'.Html::a('', '', ['class' => 'in-order-down '.$this->in_order])
							   .Html::a('', '', ['class' => 'in-order-up '.$this->in_order]); 
	}
	
	public function frontCategoryView()
	{
	  return [
	    'lider_prokata_avto_url' => $this->url,
	  ];
	}
	
	public function frontAvtoView()
	{
	  if (Yii::$app->language == 'ru')
	  return [
	    'main_image' => $this->main_image,
		'main_image_2x' => $this->main_image_2x,
		'title' => $this->title_ru,
		'subtitle' => $this->subtitle_ru,
		'main_block_price' => $this->price_ru,
		'meta_title' => $this->meta_title_ru,
		'meta_description' => $this->meta_description_ru,
		'meta_keywords' => $this->meta_keywords_ru,
	  ];
	  if (Yii::$app->language == 'en')
	  return [
	    'main_image' => $this->main_image,
		'main_image_2x' => $this->main_image_2x,
		'title' => $this->title_en,
		'subtitle' => $this->subtitle_en,
		'main_block_price' => $this->price_en,
		'meta_title' => $this->meta_title_en,
		'meta_description' => $this->meta_description_en,
		'meta_keywords' => $this->meta_keywords_en,
	  ];
	}
	
	public function backPublish() 
	{
	    $this->status = 1;
		$this->save(false);
		$avto_prices = AvtoPrice::find()->where(['avto_id' => $this->id])->all();
		foreach ($avto_prices as $one) {
		  $one->status = 1;
		  $one->save(false);
		}
		$sitemap_url = Sitemap::find()->where(['url' => $this->category_url.'/'.$this->url.'/'])->one();
		$sitemap_url->status = 1;
		$sitemap_url->save(false);
	}
	
	public function backUnpublish() 
	{
	    $this->status = 0;
		$this->save(false);
		$avto_prices = AvtoPrice::find()->where(['avto_id' => $this->id])->all();
		foreach ($avto_prices as $one) {
		  $one->status = 0;
		  $one->save(false);
		}
		$sitemap_url = Sitemap::find()->where(['url' => $this->category_url.'/'.$this->url.'/'])->one();
		$sitemap_url->status = 0;
		$sitemap_url->save(false);
	}
	
	public function backEnInfoSave($category) 
	{
	    $this->category_url = $category;
		$this->in_order = '1';
		$this->cover_description_en = $this->cover_description_ru;
		$this->title_en = $this->title_ru;
		$this->subtitle_en = $this->subtitle_ru;
		$this->price_en = $this->price_ru;
		$this->save(false);
	}
	
	public function backDownloadImagesCreate() 
	{
	  $main_image = UploadedFile::getInstance($this, 'main_image');
	  $main_image_2x = UploadedFile::getInstance($this, 'main_image_2x');
	  if (!empty($main_image)) {
		$this->main_image = $main_image;
		$main_image->saveAs(\Yii::$app->basePath.Yii::getAlias('@avtoDownload').'/'.$this->id.'/'.$main_image);
	  }
	  if (!empty($main_image_2x)) {
		$this->main_image_2x = $main_image_2x;
		$main_image_2x->saveAs(\Yii::$app->basePath.Yii::getAlias('@avtoDownload').'/'.$this->id.'/'.$main_image_2x);
	  }
	  $this->save(false);
	}
	
	public function backDownloadImagesUpdate($old_main_image, $old_main_image_2x) 
	{
	  $main_image = UploadedFile::getInstance($this, 'main_image');
	  $main_image_2x = UploadedFile::getInstance($this, 'main_image_2x');	
	  if (!empty($main_image)) {
		$this->main_image = $main_image;
		$main_image->saveAs(\Yii::$app->basePath.Yii::getAlias('@avtoDownload').'/'.$this->id.'/'.$main_image);
	  }
	  else { $this->main_image = $old_main_image; }	
	  if (!empty($main_image_2x)) {
		$this->main_image_2x = $main_image_2x;
		$main_image_2x->saveAs(\Yii::$app->basePath.Yii::getAlias('@avtoDownload').'/'.$this->id.'/'.$main_image_2x);
	  }
	  else { $this->main_image_2x = $old_main_image_2x; }
	  $this->save(false);
	}
	
	public function backDeleteIMageRu($name) 
	{
	    $main_image = $this->main_image;
		$main_image_2x = $this->main_image_2x;
		$image_path = \Yii::$app->basePath."/web/images/avto/".$this->id."/".$this->$name;
	    if (file_exists($image_path))
			unlink($image_path);
		$this->$name = '';
		if ($name == 'main_image') {
		  $this->main_image_2x = $main_image_2x;
		}
		if ($name == 'main_image_2x') {
		  $this->main_image = $main_image;
		}
		$this->save(false);
	}
	
	public function backDelete($id) 
	{
	  $avto_advantages = AvtoAdvantages::find()->where(['avto_id' => $id])->all();
	  foreach ($avto_advantages as $one) {
	    $avto_advantages_items = AvtoAdvantagesItems::find()->where(['avto_advantages_id' => $one->id])->all();
		foreach ($avto_advantages_items as $item) {
		  $item->delete();
		}
	    $one->delete();
	  }	
	  
	  $avto_experience = AvtoExperience::find()->where(['avto_id' => $id])->all();
	  foreach ($avto_experience as $one) {
	    $one->delete();
	  }
	  
	  $avto_gallery = AvtoGallery::find()->where(['avto_id' => $id])->all();
	  foreach ($avto_gallery as $one) {
	    $avto_gallery_images = AvtoGalleryImages::find()->where(['avto_gallery_id' => $one->id])->all();
		foreach ($avto_gallery_images as $image) {
		  $image->delete();
		}
	    $one->delete();
	  }
	  
	  $avto_price = AvtoPrice::find()->where(['avto_id' => $id])->all();
	  foreach ($avto_price as $one) {
	    $one->delete();
	  }
	  $all_price_after = AvtoPrice::find()->where('in_order > :in_order', [':in_order' => $this->in_order])->all();
	  foreach ($all_price_after as $one) {
	    $one->in_order = $one->in_order - 1;
		$one->save(false);
	  }
	  $avtopark_rental_lider_all = AvtoparkRentalLider::find()->where(['avto_id' => $id])->all();
	  if ($avtopark_rental_lider_all) {
	    foreach ($avtopark_rental_lider_all as $one) {
		  $one->avto_id = '0';
		  $one->save(false);
		}
	  }
	  
	  $sitemap_url = Sitemap::find()->where(['url' => $this->category_url.'/'.$this->url.'/'])->one();
	  $sitemap_url->delete();
	  
	  $this->delete();
	  $all_avto_after = Avto::find()->where('in_order > :in_order', [':in_order' => $this->in_order])->all();
	  foreach ($all_avto_after as $one) {
	    $one->in_order = $one->in_order - 1;
		$one->save(false);
	  }	
		
	  $dir = \Yii::$app->basePath."/web/images/avto/".$id;	
	  if ($objs = glob($dir."/*")) {
       foreach($objs as $obj) {
         is_dir($obj) ? removeDirectory($obj) : unlink($obj);
       }
      }
      rmdir($dir);	
	}
}
