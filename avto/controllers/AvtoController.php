<?php

namespace backend\modules\avto\controllers;

use Yii;
use backend\modules\avto\models\Avto;
use backend\modules\avto\models\AvtoSearch;
use backend\modules\avtoGallery\models\AvtoGallery;
use backend\modules\avtoGalleryImages\models\AvtoGalleryImages;
use backend\modules\avtoExperience\models\AvtoExperience;
use backend\modules\avtoAdvantages\models\AvtoAdvantages;
use backend\modules\avtoAdvantagesItems\models\AvtoAdvantagesItems;
use backend\modules\avtoPrice\models\AvtoPrice;
use backend\modules\avtoparkCategory\models\AvtoparkCategory;
use backend\modules\sitemap\models\Sitemap;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\imagine\Image;

/**
 * AvtoController implements the CRUD actions for Avto model.
 */
class AvtoController extends Controller
{
    public function behaviors()
    {
        return [
			'access' => [
              'class' => \yii\filters\AccessControl::className(),
              'only' => ['index', 'create', 'update', 'delete', 'publish', 'unpublish',
						 'update-ru', 'update-en', 'delete-image-ru', 'in-order-down',
						 'in-order-up'],
              'rules' => [
                [
                  'allow' => true,
                  'roles' => ['@'],
                ],
              ],
			],
        ];
    }


    public function actionIndex($category, $lang)
    {
		$rowCount = $_GET['rowCount'];
	    if (empty($_GET['rowCount'])) $rowCount = 10;
	
        $searchModel = new AvtoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $category, $rowCount);

		if ($lang == 'ru')
        return $this->render('index-ru', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'category' => $category,
			'lang' => $lang,
			'rowCount' => $rowCount,
        ]);
		if ($lang == 'en')
        return $this->render('index-en', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'category' => $category,
			'lang' => $lang,
			'rowCount' => $rowCount,
        ]);
    }
	
	public function actionPublish()
	{ 
	  $id = $_POST['id'];
	    $avto = $this->findModel($id);
		$avto->backPublish();
	}
	
	public function actionUnpublish()
	{ 
	  $id = $_POST['id'];
	    $avto = $this->findModel($id);
		$avto->backUnpublish();
	}
	
    public function actionCreate($category)
    {
        $model = new Avto();
		$avto_gallery = new AvtoGallery();
		$avto_experience = new AvtoExperience();
		$avto_advantages = new AvtoAdvantages();
		$avto_price = new AvtoPrice();
		$avto_advantages_items1 = new AvtoAdvantagesItems();
		$avto_advantages_items2 = new AvtoAdvantagesItems();
		
		$category_id = AvtoparkCategory::find()->where(['url' => $category])->one()->id;
		$category_order = AvtoparkCategory::find()->where(['url' => $category])->one()->in_order;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
		
		  $all_avto = Avto::find()->where(['category_url' => $category])->all();
		  foreach ($all_avto as $one) {
		    $one->in_order = $one->in_order + 1;
		    $one->save(false);
		  }  
		
		$model->backEnInfoSave($category);
		
		$avto_gallery_ru = new AvtoGallery();
		$avto_gallery_en = new AvtoGallery();
		$avto_experience_ru = new AvtoExperience();
		$avto_experience_en = new AvtoExperience();
		$avto_advantages_ru = new AvtoAdvantages();
		$avto_advantages_en = new AvtoAdvantages();
		$avto_price_ru = new AvtoPrice();
		$avto_price_en = new AvtoPrice();
		
		$new_folder = \Yii::$app->basePath.Yii::getAlias('@avtoDownload').'/'.$model->id;
		if(!is_dir($new_folder)) {
		  mkdir($new_folder,0777, true);
		  chmod($new_folder, 0777);
	    }
		
		/*** БЛОК1 > ***/
			$model->backDownloadImagesCreate();		
		/*** < БЛОК1 ***/	
		
		/*** БЛОК2 > ***/	
			$avto_gallery_ru->load(Yii::$app->request->post());
			$avto_gallery_ru->backSaveInfoCreate($model->id, 'ru');
			$avto_gallery_en->load(Yii::$app->request->post());
			$avto_gallery_en->backSaveInfoCreate($model->id, 'en');
			$avto_gallery->backDownloadImagesCreateUpdate($avto_gallery_ru->avto_id, $avto_gallery_ru->id, $model);
			
			$model->cover_description_ru = $avto_gallery_ru->description_hover;
			$model->cover_description_en = $avto_gallery_ru->description_hover;
			$model->save();			
		/*** < БЛОК2 ***/
		
		/*** БЛОК3 > ***/
			$avto_experience_ru->load(Yii::$app->request->post());
			$avto_experience_ru->backLoadInfoCreate($model->id, 'ru');
			
			$avto_experience_en->load(Yii::$app->request->post());
			$avto_experience_en->backLoadInfoCreate($model->id, 'en');
			
			$avto_experience->backDownloadImagesCreate($avto_experience_ru, $avto_experience_en, $model->id);
		/*** < БЛОК3 ***/
			
		/*** БЛОК4 > ***/
			$avto_advantages_ru->load(Yii::$app->request->post());
			$avto_advantages_ru->backLoadInfoCreate($model->id, 'ru');
			
			$avto_advantages_en->load(Yii::$app->request->post());
			$avto_advantages_en->backLoadInfoCreate($model->id, 'en');
			
			$avto_advantages->backDownloadImagesCreate($avto_advantages_ru, $avto_advantages_en, $model->id);
	
			if (!empty($_POST['AvtoAdvantagesItems']))
			$avto_advantages_items1->backLoadInfoCreate($_POST['AvtoAdvantagesItems'], $avto_advantages_ru->id);
		/*** < БЛОК4 ***/
			
		/*** БЛОК5 > ***/
		
			$all_avto_price = AvtoPrice::find()->where(['category_url' => $model->category_url])->all();
			foreach ($all_avto_price as $one) {
			  $one->in_order = $one->in_order + 1;
			  $one->save(false);
			}
			$avto_price_ru->load(Yii::$app->request->post());
			$avto_price_ru->backSaveInfoCreate($model->id, $model->category_url, $category_order, $model->in_order, 'ru');
			
			$avto_price_en->load(Yii::$app->request->post());
			$avto_price_en->backSaveInfoCreate($model->id, $model->category_url, $category_order, $model->in_order, 'en');			
		/*** < БЛОК5 ***/
		
		/*** SITEMAP > ***/
		
		$sitemap = new Sitemap();
		$sitemap->url = $model->category_url.'/'.$model->url.'/';
		$sitemap->save(false);
		
		/*** < SITEMAP ***/
		
            return $this->redirect(['update-ru', 'id' => $model->id, 'category' => $model->category_url]);
        } else {
            return $this->render('create', [
                'model' => $model,
				'category_id' => $category_id,
				'avto_gallery' => $avto_gallery,
				'avto_experience' => $avto_experience,
				'avto_advantages' => $avto_advantages,
				'avto_price' => $avto_price,
				'avto_advantages_items1' => $avto_advantages_items1,
				'avto_advantages_items2' => $avto_advantages_items2
            ]);
        }
    }

    public function actionUpdateRu($category, $id, $active=NULL)
    {
	    $category_id = AvtoparkCategory::find()->where(['url' => $category])->one()->id;
        $model = $this->findModel($id);
		$old_url = $model->url; 
		$avto_gallery = AvtoGallery::find()->where(['avto_id' => $model->id, 'lang' => 'ru'])->one();
		$avto_experience = AvtoExperience::find()->where(['avto_id' => $model->id, 'lang' => 'ru'])->one();
		$avto_experience_en = AvtoExperience::find()->where(['avto_id' => $model->id, 'lang' => 'en'])->one();		
		$avto_advantages = AvtoAdvantages::find()->where(['avto_id' => $model->id, 'lang' => 'ru'])->one();
		$avto_advantages_en = AvtoAdvantages::find()->where(['avto_id' => $model->id, 'lang' => 'en'])->one();
		$avto_advantages_items1 = AvtoAdvantagesItems::find()->where(['avto_advantages_id' => $avto_advantages->id, 'part' => '1'])->all();
		$avto_advantages_items2 = AvtoAdvantagesItems::find()->where(['avto_advantages_id' => $avto_advantages->id, 'part' => '2'])->all();		
		$avto_price = AvtoPrice::find()->where(['avto_id' => $model->id, 'lang' => 'ru'])->one();			
		$avto_gallery_images = AvtoGalleryImages::find()->where(['avto_gallery_id' => $avto_gallery->id])->orderBy('in_order')->all();
		
		$old_main_image = $model->main_image;
		$old_main_image_2x = $model->main_image_2x;

		/*** МЕТАДАННЫЕ > ***/
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
		/*** < МЕТАДАННЫЕ ***/
		
		/*** БЛОК1 > ***/
			$model->backDownloadImagesUpdate($old_main_image, $old_main_image_2x);
		/*** < БЛОК1 ***/
			
		/*** БЛОК2 > ***/
			$avto_gallery->load(Yii::$app->request->post());
			$avto_gallery->save(false);
			$avto_gallery->backDownloadImagesCreateUpdate($avto_gallery->avto_id, $avto_gallery->id, $model);
			if (!empty($_POST['AvtoGalleryImages']))
			  $avto_gallery->backSaveImagesInfoUpdate($_POST['AvtoGalleryImages']['image_alt_ru'], $_POST['AvtoGalleryImages']['image_title_ru']);
			
			$model->cover_description_ru = $avto_gallery->description_hover;
			$model->save();
		/*** < БЛОК2 ***/
			
		/*** БЛОК3 > ***/
			$old_big_image1 = $avto_experience->big_image1;
			$old_big_image2 = $avto_experience->big_image2;
			$old_small_image1 = $avto_experience->small_image1;
			$old_small_image2 = $avto_experience->small_image2;
			$old_small_image3 = $avto_experience->small_image3;
			$avto_experience->load(Yii::$app->request->post());
			$avto_experience->backDownloadImagesUpdate($old_big_image1, $old_big_image2, $old_small_image1,
													   $old_small_image2, $old_small_image3, 
													   $avto_experience_en, $model->id);
		/*** < БЛОК3 ***/
			
		/*** БЛОК4 > ***/
			$old_image1 = $avto_advantages->image1;
			$old_image2 = $avto_advantages->image2;
			$avto_advantages->load(Yii::$app->request->post());
			$avto_advantages->backDownloadImagesUpdate($old_image1, $old_image2, $avto_advantages_en, $model->id);

			if (!empty($_POST['AvtoAdvantagesItems']['part1']['item_ru']) OR
				!empty($_POST['AvtoAdvantagesItems']['part2']['item_ru']))
			$avto_advantages->saveInfoItemsUpdate($_POST['AvtoAdvantagesItems']['part1']['item_ru'],
												  $_POST['AvtoAdvantagesItems']['part2']['item_ru']);
		/*** < БЛОК4 ***/
			
		/*** БЛОК5 > ***/
			$avto_price->load(Yii::$app->request->post());
			$avto_price->category_url = $model->category_url;
			$avto_price->save(false);
		/*** < БЛОК5 ***/
		
		/*** SITEMAP > ***/
			if ($old_url != $model->url) {
			  $sitemap_url = Sitemap::find()->where(['url' => $model->category_url.'/'.$old_url.'/'])->one();
			  $sitemap_url->url = $model->category_url.'/'.$model->url.'/';
			  $sitemap_url->save(false);
			}
		/*** < SITEMAP ***/
		
            return $this->redirect(['update-ru', 'id' => $model->id, 'category' => $category]);
        } else {
            return $this->render('update-ru', [
                'model' => $model,
				'category_id' => $category_id,
				'avto_gallery' => $avto_gallery,
				'avto_gallery_images' => $avto_gallery_images,
				'active' => $active,
				'avto_price' => $avto_price,
				'avto_experience' => $avto_experience,
				'avto_advantages' => $avto_advantages,
				'avto_advantages_items1' => $avto_advantages_items1,
				'avto_advantages_items2' => $avto_advantages_items2
            ]);
        }
    }
	
	public function actionUpdateEn($category, $id, $active=NULL)
    {
        $model = $this->findModel($id);
		$avto_gallery = AvtoGallery::find()->where(['avto_id' => $model->id, 'lang' => 'en'])->one();
		$avto_experience = AvtoExperience::find()->where(['avto_id' => $model->id, 'lang' => 'en'])->one();	
		$avto_advantages = AvtoAdvantages::find()->where(['avto_id' => $model->id, 'lang' => 'en'])->one();
		$avto_advantages_ru_id = AvtoAdvantages::find()->where(['avto_id' => $model->id, 'lang' => 'ru'])->one()->id;
		$avto_advantages_items1 = AvtoAdvantagesItems::find()->where(['avto_advantages_id' => $avto_advantages_ru_id, 'part' => '1'])->all();
		$avto_advantages_items2 = AvtoAdvantagesItems::find()->where(['avto_advantages_id' => $avto_advantages_ru_id, 'part' => '2'])->all();		
		$avto_price = AvtoPrice::find()->where(['avto_id' => $model->id, 'lang' => 'en'])->one();			
		$avto_gallery_images = AvtoGalleryImages::find()->where(['avto_gallery_id' => $avto_gallery->id])->all();

		/*** МЕТАДАННЫЕ > ***/
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
		/*** < МЕТАДАННЫЕ ***/
		
			
		/*** БЛОК2 > ***/
			$avto_gallery->load(Yii::$app->request->post());
			
			$avto_gallery->save(false);
			
			if (!empty($_POST['AvtoGalleryImages']['image_alt_en']) OR 
				!empty($_POST['AvtoGalleryImages']['image_title_en']))
			$avto_gallery->backLoadInfoUpdateEn($_POST['AvtoGalleryImages']['image_alt_en'],
												$_POST['AvtoGalleryImages']['image_title_en']);
			
			$model->cover_description_en = $avto_gallery->description_hover;
			$model->save();
		/*** < БЛОК2 ***/
			
		/*** БЛОК3 > ***/
			$avto_experience->load(Yii::$app->request->post());
			$avto_experience->save(false);
		/*** < БЛОК3 ***/
			
		/*** БЛОК4 > ***/
			$avto_advantages->load(Yii::$app->request->post());
			$avto_advantages->save(false);
			if (!empty($_POST['AvtoAdvantagesItems']['part1']['item_en']) OR
				!empty($_POST['AvtoAdvantagesItems']['part2']['item_en']))
			$avto_advantages->backSaveInfoUpdateEn($_POST['AvtoAdvantagesItems']['part1']['item_en'],
												   $_POST['AvtoAdvantagesItems']['part2']['item_en']);
		/*** < БЛОК4 ***/
			
		/*** БЛОК5 > ***/
			$avto_price->load(Yii::$app->request->post());
			$avto_price->category_url = $model->category_url;
			$avto_price->save(false);
		/*** < БЛОК5 ***/
		
            return $this->redirect(['update-en', 'id' => $model->id, 'category' => $category]);
        } else {
		  
           
			return $this->render('update-en', [
                'model' => $model,
				'avto_gallery' => $avto_gallery,
				'avto_gallery_images' => $avto_gallery_images,
				'active' => $active,
				'avto_price' => $avto_price,
				'avto_experience' => $avto_experience,
				'avto_advantages' => $avto_advantages,
				'avto_advantages_items1' => $avto_advantages_items1,
				'avto_advantages_items2' => $avto_advantages_items2
            ]);
        }
    }
	
	public function actionDeleteImageRu($id, $name)
    {
	  $this->layout = false;
        $model = $this->findModel($id);
		$model->backDeleteImageRu($name);
		return $this->redirect(['update-ru', 'id' => $model->id, 'category' => $model->category_url]);
    }

	
	public function actionDelete()
    {
	  $id = $_POST['id'];
	  $model = $this->findModel($id);
	  $model->backDelete($id);
    }

    protected function findModel($id)
    {
        if (($model = Avto::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	public function actionInOrderDown()
	{
	  $in_order = $_POST['in_order'];
	  $category = $_POST['category'];
	  $previous = Avto::find()->where('in_order < :in_order AND category_url = :category', [':in_order' => $in_order, ':category' => $category])->orderBy('in_order DESC')->one();
	  $in_order_previous = $previous->in_order;
	  $next = Avto::find()->where('in_order = :in_order AND category_url = :category', [':in_order' => $in_order, ':category' => $category])->one();
	  $in_order_next = $next->in_order;
	  $previous->in_order = $in_order_next;
	  $previous->save(false);
	  $next->in_order = $in_order_previous;
	  $next->save(false);
	  $avto_prices_previous = AvtoPrice::find()->where(['avto_id' => $previous->id])->all();
		foreach ($avto_prices_previous as $one) {
		  $one->in_order = $in_order_next;
		  $one->save(false);
		}
	  $avto_prices_next = AvtoPrice::find()->where(['avto_id' => $next->id])->all();
		foreach ($avto_prices_next as $one) {
		  $one->in_order = $in_order_previous;
		  $one->save(false);
		}
	}
	
	public function actionInOrderUp()
	{
	  $in_order = $_POST['in_order'];
	  $category = $_POST['category'];
	  $next = Avto::find()->where('in_order > :in_order AND category_url = :category', [':in_order' => $in_order, ':category' => $category])->orderBy('in_order')->one();
	  $in_order_next = $next->in_order;
	  $previous = Avto::find()->where('in_order = :in_order AND category_url = :category', [':in_order' => $in_order, ':category' => $category])->one();
	  $in_order_previous = $previous->in_order;
	  $next->in_order = $in_order_previous;
	  $next->save(false);
	  $previous->in_order = $in_order_next;
	  $previous->save(false);
	  $avto_prices_previous = AvtoPrice::find()->where(['avto_id' => $previous->id])->all();
		foreach ($avto_prices_previous as $one) {
		  $one->in_order = $in_order_next;
		  $one->save(false);
		}
	  $avto_prices_next = AvtoPrice::find()->where(['avto_id' => $next->id])->all();
		foreach ($avto_prices_next as $one) {
		  $one->in_order = $in_order_previous;
		  $one->save(false);
		}
	}
	
}
