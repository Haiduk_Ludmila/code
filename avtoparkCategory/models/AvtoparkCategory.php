<?php

namespace backend\modules\avtoparkCategory\models;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\UploadedFile;
use backend\modules\avto\models\Avto;
use backend\modules\sitemap\models\Sitemap;
use backend\modules\addMenu\models\AddMenu;

/**
 * This is the model class for table "lim_avtopark_category".
 *
 * @property integer $id
 * @property string $url
 * @property string $title_ru
 * @property string $description_ru
 * @property string $icon
 * @property string $icon_alt_ru
 * @property string $icon_title_ru
 * @property string $title_en
 * @property string $description_en
 * @property string $icon_alt_en
 * @property string $icon_title_en
 * @property string $created_at
 * @property integer $in_order
 * @property string $meta_title_ru
 * @property string $meta_description_ru
 * @property string $meta_keywords_ru
 * @property string $meta_title_en
 * @property string $meta_description_en
 * @property string $meta_keywords_en 
 */
class AvtoparkCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lim_avtopark_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['url', 'title_ru', 'description_ru', 'icon', 'icon_alt_ru', 'icon_title_ru', 'title_en', 'description_en', 'icon_alt_en', 'icon_title_en', 'in_order'], 'required'],
			[['url'], 'required'],
            [['created_at'], 'safe'],
            [['in_order'], 'integer'],
            [['url', 'title_ru', 'description_ru', 'icon', 'icon_alt_ru', 'icon_title_ru', 'title_en', 'description_en', 'icon_alt_en', 'icon_title_en', 'meta_title_ru', 'meta_description_ru', 'meta_keywords_ru', 'meta_title_en', 'meta_description_en', 'meta_keywords_en'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'url' => 'Url категории в адресной строке',
            'title_ru' => 'Название категории(h3)',
            'description_ru' => 'Описание категории. Рекомендуется 60 зн.',
            'icon' => 'Иконка',
            'icon_alt_ru' => 'Описание иконки(Alt)',
            'icon_title_ru' => 'Описание иконки(Title)',
            'title_en' => 'Название категории(h3)',
            'description_en' => 'Описание категории. Рекомендуется 60 зн.',
            'icon_alt_en' => 'Описание иконки(Alt)',
            'icon_title_en' => 'Описание иконки(Title)',
            'created_at' => 'Created At',
            'in_order' => 'In Order',
			'meta_title_ru' => 'Заголовок страницы (Title)',
            'meta_description_ru' => 'Описание страницы (Description)',
            'meta_keywords_ru' => 'Ключевые слова страницы (Keywords)',
            'meta_title_en' => 'Заголовок страницы (Title)',
            'meta_description_en' => 'Описание страницы (Description)',
            'meta_keywords_en' => 'Ключевые слова страницы (Keywords)',
        ];
    }
	
	public function arrowOrder() {
		$model_last = AvtoparkCategory::find()->orderBy('in_order desc')->one();
		$model_id_last = $model_last->id;
		$model_first = AvtoparkCategory::find()->orderBy('in_order asc')->one();
		$model_id_first = $model_first->id;
		if ($model_id_first == $model_id_last)
        return '<span class="span-in-order inlineEdit '.$this->in_order.'">'.$this->in_order.'</span>';
		if ($model_id_first == $this->id)
        return '<span class="span-in-order inlineEdit '.$this->in_order.'">'.$this->in_order.'</span>'.Html::a('', '', ['class' => 'in-order-up '.$this->in_order]);
		if ($model_id_last == $this->id)
        return '<span class="span-in-order inlineEdit '.$this->in_order.'">'.$this->in_order.'</span>'.Html::a('', '', ['class' => 'in-order-down '.$this->in_order]);
		if (($model_id_first != $this->id) && ($model_id_last != $this->id))
        return '<span class="span-in-order inlineEdit '.$this->in_order.'">'.$this->in_order.'</span>'.Html::a('', '', ['class' => 'in-order-down '.$this->in_order])
							   .Html::a('', '', ['class' => 'in-order-up '.$this->in_order]); 
	}
	
	public function frontCategoryView()
	{
	  if (Yii::$app->language == 'ru')
	  return [
		'category_id' => $this->id,
		'category_url' => $this->url,
		'meta_title' => $this->meta_title_ru,
		'meta_description' => $this->meta_description_ru,
		'meta_keywords' => $this->meta_keywords_ru,
	  ];
	  if (Yii::$app->language == 'en')
	  return [
		'category_id' => $this->id,
		'category_url' => $this->url,
		'meta_title' => $this->meta_title_en,
		'meta_description' => $this->meta_description_en,
		'meta_keywords' => $this->meta_keywords_en,
	  ];
	}
	
	public function backDownloadIcon($old_icon) 
	{
	    $icon = UploadedFile::getInstance($this, 'icon');	
			if (!empty($icon)) {
			  $this->icon = $icon;
		      $icon->saveAs(\Yii::$app->basePath.Yii::getAlias('@categoryDownload').'/'.$this->id.'/'.$icon);
			}
			else { $this->icon = $old_icon; }
		$this->save(false);	
	}
	
	public function backChangeDependentUrls($old_url, $new_url) 
	{
		$all_avto = Avto::find()->where(['url' => $old_url])->all();
		foreach ($all_avto as $one) {
		  $one->category_url = $new_url;
		  $one->save(false);
		}
		$sitemap_urls = Sitemap::find()->all();
		foreach ($sitemap_urls as $one) {
		  $one->url = str_replace($old_url, $new_url, $one->url);
		  $one->save(false);
		} 
		$add_menu = AddMenu::find()->all();
		foreach ($add_menu as $one) {
		  $one->action_name = str_replace($old_url, $new_url, $one->action_name);
		  $one->add_action_url = str_replace($old_url, $new_url, $one->add_action_url);
	      $one->save(false);
		}
	}
	
	public function backChangeDependentTitle($old_title, $new_title) 
	{
		$add_menu = AddMenu::find()->all();
		foreach ($add_menu as $one) {
		  $one->title = str_replace($old_title, $new_title, $one->title);
		  $one->save(false);
		}
	}		  
	
	public function backDeleteIcon() 
	{
	    $image_path = \Yii::$app->basePath."/web/images/category/".$this->id."/".$this->icon;
	    if (file_exists($image_path))
			unlink($image_path);
		$this->load(Yii::$app->request->post());
		$this->icon = '';
		$this->save(false);
	}
	
}
