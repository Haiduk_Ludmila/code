<?php

namespace backend\modules\avtoparkCategory\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\avtoparkCategory\models\AvtoparkCategory;

/**
 * AvtoparkCategorySearch represents the model behind the search form about `backend\modules\avtoparkCategory\models\AvtoparkCategory`.
 */
class AvtoparkCategorySearch extends AvtoparkCategory
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'in_order'], 'integer'],
            [['url', 'title_ru', 'description_ru', 'icon', 'icon_alt_ru', 'icon_title_ru', 'title_en', 'description_en', 'icon_alt_en', 'icon_title_en', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AvtoparkCategory::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'in_order' => $this->in_order,
        ]);

        $query->andFilterWhere(['like', 'url', $this->url])
            ->andFilterWhere(['like', 'title_ru', $this->title_ru])
            ->andFilterWhere(['like', 'description_ru', $this->description_ru])
            ->andFilterWhere(['like', 'icon', $this->icon])
            ->andFilterWhere(['like', 'icon_alt_ru', $this->icon_alt_ru])
            ->andFilterWhere(['like', 'icon_title_ru', $this->icon_title_ru])
            ->andFilterWhere(['like', 'title_en', $this->title_en])
            ->andFilterWhere(['like', 'description_en', $this->description_en])
            ->andFilterWhere(['like', 'icon_alt_en', $this->icon_alt_en])
            ->andFilterWhere(['like', 'icon_title_en', $this->icon_title_en]);

        /*return $dataProvider;*/
		
		return $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'sort'=> ['defaultOrder' => ['in_order'=>SORT_ASC],
					  'attributes' => ['in_order' => [
                        'asc' => ['in_order' => SORT_ASC],
                        'desc' => [],
						'label'=>'Order Name'
						],
						'title_ru',
						'created_at',
					  ]],		  
        ]);
    }
}
