<?php

namespace backend\modules\avtoparkCategory\controllers;

use Yii;
use backend\modules\avtoparkCategory\models\AvtoparkCategory;
use backend\modules\avtoparkCategory\models\AvtoparkCategorySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * avtoparkCategoryController implements the CRUD actions for avtoparkCategory model.
 */
class AvtoparkCategoryController extends Controller
{
    public function behaviors()
    {
        return [
			'access' => [
              'class' => \yii\filters\AccessControl::className(),
              'only' => ['index', 'update', 'delete',
						 'delete-icon', 'meta', 'in-order-down', 'in-order-up'],
              'rules' => [
                [
                  'allow' => true,
                  'roles' => ['@'],
                ],
              ],
			],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new avtoparkCategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
		$old_icon = $model->icon;
		$old_url = $model->url;
		$old_title = $model->title_ru;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
			$model->backDownloadIcon($old_icon);		
			if ($old_url != $model->url)
			  $model->backChangeDependentUrls($old_url, $model->url);		
			if ($old_title != $model->title_ru)
			  $model->backChangeDependentTitle($old_title, $model->title_ru);
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
				'id' => $model->id
            ]);
        }
    }
	
	public function actionMeta($category, $lang)
    {
        $model = AvtoparkCategory::find()->where(['url' => $category])->one();
		
		if ($model->load(Yii::$app->request->post()) && $model->save()) {
		  return $this->redirect(['meta', 'category' => $category, 'lang' => $lang]);
		}
		else {
            return $this->render('meta', [
                'model' => $model,
				'id' => $model->id,
				'lang' => $lang
            ]);
        }
		
	}	
	
	public function actionDeleteIcon()
    { 
	  $this->layout = false;
	  $id = $_POST['AvtoparkCategory']['id'];
      $model = AvtoparkCategory::find()->where(['id' => $id])->one();
	  $model->backDeleteIcon();
	  return $this->redirect(['update', 'id' => $id]);
    }
	
	public function actionInOrderDown()
	{
	  $in_order = $_POST['in_order'];
	  $previous = AvtoparkCategory::find()->where('in_order < :in_order', [':in_order' => $in_order])->orderBy('in_order DESC')->one();
	  $in_order_previous = $previous->in_order;
	  $next = AvtoparkCategory::find()->where('in_order = :in_order', [':in_order' => $in_order])->one();
	  $in_order_next = $next->in_order;
	  $previous->in_order = $in_order_next;
	  $previous->save(false);
	  $next->in_order = $in_order_previous;
	  $next->save(false);
	}
	
	public function actionInOrderUp()
	{
	  $in_order = $_POST['in_order'];
	  $next = AvtoparkCategory::find()->where('in_order > :in_order', [':in_order' => $in_order])->orderBy('in_order')->one();
	  $in_order_next = $next->in_order;
	  $previous = AvtoparkCategory::find()->where('in_order = :in_order', [':in_order' => $in_order])->one();
	  $in_order_previous = $previous->in_order;
	  $next->in_order = $in_order_previous;
	  $next->save(false);
	  $previous->in_order = $in_order_next;
	  $previous->save(false);
	}

    protected function findModel($id)
    {
        if (($model = AvtoparkCategory::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
